package group;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ListsLoop {
	
	static public void main (String[] args) {
		ListsLoop listsLoop = new ListsLoop();
		
		List<String> list1 = Arrays.asList(new String[] {
				"A", "B", "C"
		});
		List<String> list2 = Arrays.asList(new String[] {
				"1", "2"
		});
		List<String> list3 = Arrays.asList(new String[] {
				"ㄅ", "ㄆ"
		});
		
		listsLoop.printLoop(list1, list2, list3);
	}
	
	public void printLoop(List<String>... lists) {
		System.out.println("START");
		int[] listsEachLen = new int[lists.length];
		int total = 0;
		int cnt = 0;
		for(List<String> list : lists) {
			for(String s : list)
				System.out.println(s);
			listsEachLen[cnt++] = list.size();
			total += list.size();
		}
		
//		for(int i : listsEachLen)
//			System.out.println(i);
		
		
//		lists[0].get(0) + lists[1].get(0)
		
		while(total > 0) {
			String combine = "";
			for(int listsCnt = 0; listsCnt < lists.length; listsCnt++) {
				int eachIdx = lists[listsCnt].size() - listsEachLen[listsCnt];
				combine += lists[listsCnt].get(eachIdx);
			}
			System.out.println(combine);
			
			listsEachLen[lists.length-1]--;
			
//			for(int listsCnt = lists.length-1; listsCnt > 0; listsCnt--) {
//				
//			}
			
			total--;
		}
	//		
		System.out.println("END");
	}
}
