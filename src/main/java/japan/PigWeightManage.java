package japan;

import java.util.Arrays;

import medium.Encryption;

public class PigWeightManage {
//	2 10 20
//	5 6
//	3 10
	
//	3
	
	static public void main (String[] args) {
		PigWeightManage pwm = new PigWeightManage();
//		int[][] input = new int[][]{
//			{2, 10, 20},
//			{5, 6},
//			{3, 10}
//		};
//		long rs = pwm.pigWeightMangeImprove(input);
//		System.out.println("rs: " + rs);	// 3
		
//		int[][] input2 = new int[][]{
//			{8, 300, 400},
//			{9, 39},
//			{48, 38},
//			{21, 10},
//			{14, 45},
//			{32, 20},
//			{32, 48},
//			{9, 7},
//			{19, 16}
//		};
//		int expect2 = 194;
//		long rs2 = pwm.pigWeightMange(input2);
//		System.out.println("rs2: " + rs2);
//		long rs2Improve = pwm.pigWeightMangeImprove(input2);
//		System.out.println("rs2Improve: " + rs2Improve);
		
		int[][] input3 = new int[][]{
			{4, 300, 310},
			{9, 39},
			{48, 38},
			{21, 10},
			{14, 45}
		};
//		int expect3 = 9;
//		long rs3 = pwm.pigWeightMange(input3);
//		System.out.println("rs3: " + rs3);
//		long rs3Improve = pwm.pigWeightMangeImprove(input3);
//		System.out.println("rs3Improve: " + rs3Improve);
//		System.out.println(pwm.getDayAccs(input3));
//		Arrays.asList(pwm.getDayAccs(input3)).stream()
//			.forEach(i->System.out.println(i+""));
		for(int i : pwm.getDayAccs(input3))
			System.out.println(i);
    }
	
	public long pigWeightMange(int[][] input) {
		
		int[] initVal = input[0];
		int[][] dayVal = Arrays.copyOfRange(input, 1, input.length);
		
		int dayNum = initVal[0];
		int limitWeight = initVal[2];
		int[] lastWeights = new int[]{initVal[1]};
		int aliveCnt = 0;
		
		for (int currDay=0; currDay<dayNum; currDay++) {
			int currDayMinusWeight = dayVal[currDay][0];
			int currDayAddWeight = dayVal[currDay][1];
			
			int[] saveWeights = new int[(int)Math.pow(2, (currDay+1))];
			aliveCnt = 0;
//			System.out.println("----");
//			System.out.println("currDay(+1): " + (currDay+1));
			
			for (int weight : lastWeights) {
				if (weight == 0)
					break;
				int smallerWeight = weight - currDayMinusWeight;
				int biggerWeight = weight + currDayAddWeight;
				
				if (smallerWeight > 0) {
					saveWeights[aliveCnt] = smallerWeight;
					aliveCnt++;
				}
				if (biggerWeight <= limitWeight) {
					saveWeights[aliveCnt] = biggerWeight;
					aliveCnt++;
				}
			}
			lastWeights = saveWeights;
			
//			for (int w : lastWeights)
//				System.out.print(w + " ");
//			System.out.println("\naliveCnt: " + aliveCnt);
		}
		
		return aliveCnt;
	}
	
	public int[] getDayAccs(int[][] input) {
		int[] initVal = input[0];
		int dayTotal = initVal[0];
		int[][] dayVal = Arrays.copyOfRange(input, 1, input.length);
		int[] dayAccs = new int[dayTotal-1];
		for (int day=(dayAccs.length-1); day>=0; day--) {
			if (day == (dayAccs.length-1))
				dayAccs[day] = dayVal[day+1][1];
			else
				dayAccs[day] = dayAccs[day+1] + dayVal[day+1][1];
		}
		
		return dayAccs;
	}
	
//	public int[] getDayAccsImprove(int[][] input) {
//		int[] initVal = input[0];
//		int dayTotal = initVal[0];
//		int[][] dayVal = Arrays.copyOfRange(input, 1, input.length);
//		int[] dayAccs = new int[dayTotal-1];
//		Arrays.asList(dayVal).stream()
//			.
//		
//		return dayAccs;
//	}
	
	public long pigWeightMangeImprove(int[][] input) {
		
		int[] initVal = input[0];
		int[][] dayVal = Arrays.copyOfRange(input, 1, input.length);
		
		int dayTotal = initVal[0];
		int baseWeight = initVal[1];
		int limitWeight = initVal[2];
		long outCnt = 0;
		
		long aliveNum = (long)Math.pow(2, dayTotal);
		System.out.println("aliveNum: " + aliveNum + ", limitWeight: " + limitWeight);
		
		int[] dayAccs = new int[dayTotal-1];
		for (int day=(dayAccs.length-1); day>=0; day--) {
			if (day == (dayAccs.length-1))
				dayAccs[day] = dayVal[day+1][1];
			else
				dayAccs[day] = dayAccs[day+1] + dayVal[day+1][1];
		}
		
		for (int d : dayAccs)
			System.out.println("d: " + d);
		
		int dayCnt = 0;
		int currWeight = baseWeight;
		int preWeight = baseWeight;
		while (dayCnt < dayTotal) {
			
			currWeight += dayVal[dayCnt][1];
			
			System.out.print(currWeight + " ");
			
			if (currWeight > limitWeight) {
				long outNum = (long)Math.pow(2, dayTotal-(dayCnt+1));
				outCnt += outNum;
				System.out.print("out!(" + outNum + ") ");
//				System.out.print("compx: " + ((preWeight-dayVal[dayCnt][0])+dayAccs[dayCnt]) + ", ");
				if (dayCnt == 0) {
//					System.out.println("\nbaseWeight: " + baseWeight);
//					System.out.println("dayVal[dayCnt][0]: " + dayVal[dayCnt][0]);
					currWeight = baseWeight -= dayVal[dayCnt][0];	
//					System.out.println("\ncurrWeight: " + currWeight);
//					dayCnt++;
				} else if (dayCnt < (dayTotal-1)) {
					System.out.print("\npreWeight: " + preWeight + ", ");
					System.out.print("dayVal[dayCnt][0]): " + dayVal[dayCnt][0] + ", ");
					System.out.print("dayAccs[dayCnt]: " + dayAccs[dayCnt] + ", ");
					System.out.print("compx: " + ((preWeight-dayVal[dayCnt][0])+dayAccs[dayCnt]) + "\n");
					if ((preWeight-dayVal[dayCnt-1][0])+dayAccs[dayCnt] <= limitWeight) {
						System.out.println(" if ");
						currWeight = preWeight -= dayVal[dayCnt-1][1];	// 300
						currWeight -= dayVal[dayCnt-1][0];	// 291
//						currWeight += dayVal[dayCnt][1];
						dayCnt--;
					}
				}
			}
			
			preWeight = currWeight;
			dayCnt++;
		}
		
		aliveNum -= outCnt;
		System.out.println();
		
		return aliveNum;
	}
}
