package easy;

import java.util.HashMap;
import java.util.Map;

public class TwoSum {
    static public void main (String[] args) {

    }

    public int[] twoSum(int[] nums, int target) {
        for (int i=0; i<nums.length; i++) {
            int diff = target - nums[i];
            for (int j=0; j<nums.length; j++) {
                if (nums[j] == diff && j != i) return new int[]{i, j};
            }
        }

        return new int[]{};
    }

    public int countTwoSumImproved(int[] nums, int target) {
        int count = 0;
        Map<Integer, Integer> cache = new HashMap<>();

        for (int i=0; i<nums.length; i++) {
            int diff = target - nums[i];
            if (cache.containsKey(diff)) {
                count += cache.get(diff);
            }

            if (cache.containsKey(nums[i])) {
                cache.put(nums[i], cache.get(nums[i]).intValue()+1);
            } else {
                cache.put(nums[i], 1);
            }

        }

        return count;
    }

    public int countTwoSum(int[] nums, int target) {
        int count = 0;
        for (int i=0; i<nums.length; i++) {
            int diff = target - nums[i];
            for (int j=i+1; j<nums.length; j++) {
                if (diff == nums[j]) count++;
            }
        }
        return count;
    }
}
