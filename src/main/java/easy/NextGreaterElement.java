package easy;

import java.util.HashMap;
import java.util.Map;
import java.util.Stack;

public class NextGreaterElement {

    // O(n*k) => O(n^2) time
    // O(1+1+1+1) => O(1) space
    public int[] base(int[] subNums, int[] nums) {
        for(int i=0; i<subNums.length; i++) {
            boolean isFindPosition = false;
            int tempNum = subNums[i];
            for(int j=0; j<nums.length; j++) {
                if(isFindPosition && nums[j] > subNums[i]) {
                    subNums[i] = nums[j];
                    break;
                }
                if(!isFindPosition && nums[j] == subNums[i]) isFindPosition = true;
            }
            if(subNums[i] == tempNum) subNums[i] = -1;
        }

        return subNums;
    }

    // O(n+k*n/2) => O(n^2/2) time
    // O(n+3) => O(n) space
    public int[] baseImprovedCache(int[] subNums, int[] nums) {
        Map<Integer, Integer> numsCache = new HashMap<>();
        for(int i=0; i<nums.length; i++) {
            numsCache.put(nums[i], i);
        }

        for(int i=0; i<subNums.length; i++) {
            int tempNum = subNums[i];
            for(int j=numsCache.get(subNums[i])+1; j<nums.length; j++) {
                if(nums[j] > subNums[i]) {
                    subNums[i] = nums[j];
                    break;
                }
            }
            if(subNums[i] == tempNum) subNums[i] = -1;
        }

        return subNums;
    }

    // O(2n+k) => O(n) time
    // O(n+n) => O(n) space
    public int[] baseImproved(int[] subNums, int[] nums) {
        Map<Integer, Integer> dictionary = new HashMap<>();
        Stack<Integer> stack = new Stack<>();

        for(int i=0; i<nums.length; i++) {
            while(stack.size() > 0 && stack.peek() < nums[i]) {
                dictionary.put(stack.pop(), nums[i]);
            }
            stack.push(nums[i]);
        }

        for(int i=0; i<subNums.length; i++)
            subNums[i] = dictionary.getOrDefault(subNums[i], -1);

        return subNums;
    }
}
