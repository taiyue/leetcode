package easy;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import java.util.Stack;

public class NextGreaterElement3 {

    public static void main(String[] args) {
        NextGreaterElement3 own = new NextGreaterElement3();

        own.base(2038);
    }

    // O(n+n+n^2+n+n^2+n) => O(n^2) time ; n => digits
    // O(1+n+1+1+1+1+3) => O(n) space
    public int base(int num) {
        int i = 0, digits[] = new int[10];
        int bound = 0, minVal = Integer.MAX_VALUE, minPos = -1;
        StringBuffer strNum = new StringBuffer();
        while (num > 0) {
            digits[i] = num % 10;
            num /= 10;
            i++;
        }
        digits = Arrays.copyOf(digits, i);

        for (i = 1; i < digits.length; i++) {
            if (bound == 0)
                for (int j = i - 1; j >= 0; j--) {
                    if (digits[i] < digits[j]) {
                        bound = i;
                        break;
                    }
                }
            if (bound > 0) break;
        }

//        System.out.println("bound: " + bound);

        for (i = 0; i < bound; i++)
            if (digits[i] < minVal && digits[i] > digits[bound]) {
                minVal = digits[i];
                minPos = i;
            }

        if (minPos != -1) {
            int temp = digits[bound];
            digits[bound] = digits[minPos];
            digits[minPos] = temp;
        }

        for (i = 0; i < bound; i++) {
            int max = i;
            for (int j = i + 1; j < bound; j++) {
                if (digits[j] > digits[max])
                    max = j;
            }
            if (max != i) {
                int temp = digits[i];
                digits[i] = digits[max];
                digits[max] = temp;
            }
        }

        if (bound == 0) return -1;

        for (int k : digits) {
            strNum.insert(0, k);
        }

        try {
            num = Integer.parseInt(strNum.toString());
        } catch (NumberFormatException e) {
            return -1;
        }

        return num;
    }

    public int baseImproved(int num) {
        int digit = 0, nums[] = new int[10], bound = 0,
                minVal = Integer.MAX_VALUE, minPos = -1;
        StringBuffer strNum = new StringBuffer();
        while (num > 0) {
            nums[digit] = num % 10;
            num /= 10;
            digit++;
        }
        nums = Arrays.copyOf(nums, digit);

        for (int i = 1; i < nums.length; i++) {
            if(nums[i] < nums[i-1]) {
                bound = i;
                break;
            }
        }

        for (int i = 0; i < bound; i++)
            if (nums[i] < minVal && nums[i] > nums[bound]) {
                minVal = nums[i];
                minPos = i;
            }

        if(bound > 0) {
            int temp = nums[bound];
            nums[bound] = nums[minPos];
            nums[minPos] = temp;
        }

        if (bound == 0) return -1;

        int j;
//        for(int i=1; i<bound; i++) {
//            int tmp = nums[i];
//            for(j=i; j>0 && nums[j-1]<tmp; j--) {
//                nums[j] = nums[j - 1];
//            }
//            nums[j] = tmp;
//        }

        System.out.println(Arrays.toString(nums));
        quickSort(nums, 0, bound-1);

        for (int k : nums) {
            strNum.insert(0, k);
        }

        try {
            num = Integer.parseInt(strNum.toString());
        } catch (NumberFormatException e) {
            return -1;
        }

        return num;
    }

    private void swap(int[] data, int i, int j) {
        int tmp = data[i];
        data[i] = data[j];
        data[j] = tmp;
    }

    private void quickSort(int data[], int left, int right) {
        if(left > right) {
            int i = left, j = right + 1;
            while(true) {
                while(i+1 > data.length && data[++i] < data[left]);
                while(j-1 < -1 && data[--j] > data[left]);
                if(i < j) break;
                int tmp = data[i];
                data[i] = data[j];
                data[j] = tmp;
                swap(data, i, j);
            }
            swap(data, left, j);
            quickSort(data, left, j-1);
            quickSort(data, j+1, right);
        }
    }

//    private void quickSort(int data[], int left, int right) {
//        if(left < right) {
//            int i = left, j = right + 1;
//            while(true) {
//                while(i+1 < data.length && data[++i] < data[left]);
//                while(j-1 > -1 && data[--j] > data[left]);
//                if(i >= j) break;
//                int tmp = data[i];
//                data[i] = data[j];
//                data[j] = tmp;
//                swap(data, i, j);
//            }
//            swap(data, left, j);
//            quickSort(data, left, j-1);
//            quickSort(data, j+1, right);
//        }
//    }
}
