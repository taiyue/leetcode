package easy;

import java.util.Arrays;
import java.util.Stack;

public class NextGreaterElement2 {

    // O(n^2) time
    // O(n+1) => O(n) space
    public int[] base(int[] nums) {

        int rs[] = new int[nums.length];

        for(int i=0; i<nums.length; i++) {
            boolean isFound = false;
            for(int j=i+1; j<nums.length; j++) {
                if(nums[i] < nums[j]) {
                    rs[i] = nums[j];
                    isFound = true;
                    break;
                }
            }
            if(!isFound)
                for(int j=0;j<i+1; j++) {
                    if(nums[i] < nums[j]) {
                        rs[i] = nums[j];
                        isFound = true;
                        break;
                    }
                }
            if(!isFound) rs[i] = -1;
        }

        return rs;
    }

    // O(n+n+2n+n) => O(n) time
    // O(1+2n+n+n+n) => O(n) space
    public int[] baseImproved(int[] nums) {
        int orgSize = nums.length, pool[] = Arrays.copyOf(nums, nums.length*2);
        System.arraycopy(nums, 0, pool, nums.length, nums.length);
        nums = new int[pool.length];
        Arrays.fill(nums, -1);
        Stack<Integer> stack = new Stack<>();
        for(int i=0; i<pool.length; i++) {
            while(stack.size() > 0 && pool[stack.peek()] < pool[i])
                nums[stack.pop()] = pool[i];
            stack.push(i);
        }
        nums = Arrays.copyOf(nums, orgSize);
        return nums;
    }

    // O(1+n+3n) => O(n) time
    // O(1+n+n+1) => O(n) space
    public int[] baseImproved2(int[] nums) {
        int orgSize = nums.length, rs[] = new int[orgSize];
        Arrays.fill(rs, -1);
        Stack<Integer> stack = new Stack<>();
        for(int i = 0; i < orgSize * 2; i++) {
            int num = nums[i % orgSize];
            while(!stack.isEmpty() && nums[stack.peek()] < num)
                rs[stack.pop()] = num;
            if(i < orgSize) stack.push(i);
        }
        return rs;
    }
}
