package coding_game;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Scanner;
import java.util.Set;

public class MazeExits {

    static public void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int W = in.nextInt();
        int H = in.nextInt();
        int X = in.nextInt();
        int Y = in.nextInt();
        if (in.hasNextLine()) {
            in.nextLine();
        }
        for (int i = 0; i < H; i++) {
            String R = in.nextLine();
            System.out.println("R: " + R);
        }

        // Write an action using System.out.println()
        // To debug: System.err.println("Debug messages...");

        System.out.println("answer");
    }

    public int[][] solve(int[] inputArea, int[] inputStart, String[] maze) {
        int W = inputArea[0];
        int H = inputArea[1];
        int X = inputStart[0];
        int Y = inputStart[1];

        Set<String> exits = new HashSet<>();
        System.out.println(37);
        exits = explore(maze, X, Y, exits, "");
        System.out.println("exits: " + exits);

        int[][] output = new int[exits.size()+1][];
        output[0] = new int[] {exits.size()};

        int cnt = 1;
        for (String e : exits) {
            System.out.println(e);
            output[cnt++] = new int[] {Integer.parseInt(e.split("_")[0]),
                    Integer.parseInt(e.split("_")[1])};
        }

        return output;
    }

    public Set<String> explore(String[] maze, int x, int y, Set<String> exits, String from) {
        System.out.println(+ x + ", " + y);

        if (maze[y].charAt(x) == '.') {
            if (x == 0 || x == maze[0].length()-1
                    || y == 0 || y == maze.length-1) {
                System.out.println("exit: " + x + ", " + y);
                exits.add(x + "_" + y);
            } else {
                if (!"left".equals(from))
                    explore(maze, x-1, y, exits, "right");
                if (!"right".equals(from))
                    explore(maze, x+1, y, exits, "left");
                if (!"up".equals(from))
                    explore(maze, x, y-1, exits, "down");
                if (!"down".equals(from))
                    explore(maze, x, y+1, exits, "up");
            }
        }
        else {
            System.out.println("else - x: " + x + ", y: " + y + ", c: " + maze[y].charAt(x));
        }

        return exits;
    }
}
