package coding_game;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Scanner;

public class RubiksCube111 {
	
	HashMap<String, Integer> diceName2CodeHm = this.createDiceName2CodeMap();
	HashMap<Integer, String> idx2NamePositive = this.createIdx2NamePositiveMap();
	HashMap<Integer, String> idx2NameReverse = this.createIdx2NameReverseMap();
	
	private HashMap<String, Integer> createDiceName2CodeMap() {
		HashMap<String, Integer> diceName2CodeHm = new HashMap<>();
		diceName2CodeHm.put("U", 1);
		diceName2CodeHm.put("D", -1);
		diceName2CodeHm.put("F", 2);
		diceName2CodeHm.put("B", -2);
		diceName2CodeHm.put("L", 3);
		diceName2CodeHm.put("R", -3);
		return diceName2CodeHm;
	}
	
	private HashMap<Integer, String> createIdx2NamePositiveMap() {
		HashMap<Integer, String> idx2NamePositive = new HashMap<>();
		idx2NamePositive.put(0, "U");
		idx2NamePositive.put(1, "F");
		idx2NamePositive.put(2, "L");
		return idx2NamePositive;
	}
	
	private HashMap<Integer, String> createIdx2NameReverseMap() {
		HashMap<Integer, String> idx2NameReverse = new HashMap<>();
		idx2NameReverse.put(0, "D");
		idx2NameReverse.put(1, "B");
		idx2NameReverse.put(2, "R");
		return idx2NameReverse;
	}
	
	static public void main(String[] args) {
		RubiksCube111 cube = new RubiksCube111();
		cube.rotationManual();
	}
	
	public void rotationManual() {
		Scanner in = new Scanner(System.in);
        String rotations = in.nextLine();
        String face1 = in.nextLine();
        String face2 = in.nextLine();
		
		rotationConsoleLog(new String[] {rotations, face1, face2});
	}
	
	public void rotationConsoleLog(String[] input) {
		char[] answerArr = rotation(input);
		for (char ans : answerArr) {
			System.out.println(ans);
		}
	}
	
	public char[] rotation(String[] input) {
		String[] rotateDirections = input[0].split(" ");
		String observeFace1 = input[1];
		String observeFace2 = input[2];
		
		int[] faces = {1, 2, 3};
		
		for(String rotateDirection : rotateDirections) {
			oneRotation(rotateDirection, faces);
		}
		
		char newFace1 = getObservePosition(faces, observeFace1);
		char newFace2 = getObservePosition(faces, observeFace2);
		
		return new char[] {newFace1, newFace2};
	}
	
	public char getObservePosition(int[] faces, String observeFace) {
		String newFace = "N";
		
		int orgFaceVal = diceName2CodeHm.get(observeFace); 
		
		for (int cnt = 0; cnt<faces.length; cnt++) {
			if (faces[cnt]==orgFaceVal) {
				newFace = idx2NamePositive.get(cnt);
				break;
			} else if (faces[cnt]+orgFaceVal==0) {
				newFace = idx2NameReverse.get(cnt);
				break;
			}
		}
		
		return newFace.charAt(0);
	}
	
	public void oneRotation(String rotateDirection, int[] faces) {
		boolean isOpposite = false;
		if (rotateDirection.indexOf('\'') > -1) 
			isOpposite = true;
		if (rotateDirection.indexOf('x') > -1)
			rotateX(isOpposite, faces);
		else if (rotateDirection.indexOf('y') > -1)
			rotateY(isOpposite, faces);
		else if (rotateDirection.indexOf('z') > -1)
			rotateZ(isOpposite, faces);
	}
	
	public void rotateX(boolean isOpposite, int[] faces) {
		rotateDirection(isOpposite, faces, 0, 1);
	}
	
	public void rotateY(boolean isOpposite, int[] faces) {
		rotateDirection(isOpposite, faces, 2, 1);
	}
	
	public void rotateZ(boolean isOpposite, int[] faces) {
		rotateDirection(isOpposite, faces, 0, 2);
	}
	
	public void rotateDirection(boolean isOpposite, int[] faces, int firstFaceIdx, int secendFaceIdx) {
		int tmp = faces[firstFaceIdx];
		if (isOpposite) {
			faces[firstFaceIdx] = -faces[secendFaceIdx];
			faces[secendFaceIdx] = tmp;
		} else {
			faces[firstFaceIdx] = faces[secendFaceIdx];
			faces[secendFaceIdx] = -tmp;
		}
	}
}
