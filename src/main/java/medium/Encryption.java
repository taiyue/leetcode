package medium;

public class Encryption {
	
	static public void main (String[] args) {
		Encryption enc = new Encryption();
		String input = "haveaniceday";
		enc.encryption(input);
    }
	
	public String encryption(String s) {
        int sLen = s.length();
        double sqrt = Math.sqrt(sLen);
        int min = (int)Math.floor(sqrt);
        int max = (int)Math.ceil(sqrt);

        if (sLen > min*max)
            min = max;

        String encryptedMsg = "";
        int idx;

        for (int c=0; c<max; c++) {
            if (c > 0)
                encryptedMsg += " ";
            for (int r=0; r<min; r++) {
                idx = r*max+c;
                if (idx < sLen)
                    encryptedMsg += s.charAt(idx);
                else
                    break;
            }
        }
        
//        System.out.print(encryptedMsg);
        return encryptedMsg;
	}
}
