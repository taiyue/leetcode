package com.ty.coding_game;

import coding_game.MazeExits;
import org.junit.Test;

import static org.junit.Assert.assertArrayEquals;

public class MazeExitsTest {

    MazeExits mazeExits = new MazeExits();

    @Test
    public void testSolve0() {
        int[] inputArea = {3, 3};
        int[] inputStart = {1, 1};
        String[] maze = {
                "###",
                "#..",
                "###"
        };
        int[][] expecteds = {
                {1},
                {1, 2}
        };
        int[][] actuals = mazeExits.solve(inputArea, inputStart, maze);
        assertArrayEquals(expecteds, actuals);
    }

    @Test
    public void testSolve1() {
        int[] inputArea = {7, 7};
        int[] inputStart = {1, 1};
        String[] maze = {
            "#######",
            "#.....#",
            "#####.#",
            "#.#...#",
            "#.#.###",
            "#......",
            "#######"
        };
        int[][] expecteds = {
                {1},
                {6, 5}
            };
        int[][] actuals = mazeExits.solve(inputArea, inputStart, maze);
        assertArrayEquals(expecteds, actuals);
    }

    @Test
    public void testSolve2() {
        int[] inputArea = {5, 5};
        int[] inputStart = {1, 1};
        String[] maze = {
                "###.#",
                "#...#",
                "###..",
                "#####",
                "....."
        };
        int[][] expecteds = {
                {2},
                {3, 0},
                {4, 2}
        };
        int[][] actuals = mazeExits.solve(inputArea, inputStart, maze);
        assertArrayEquals(expecteds, actuals);
    }

    @Test
    public void testSolve3() {
        int[] inputArea = {11, 11};
        int[] inputStart = {5, 5};
        String[] maze = {
                "###########",
                "......#...#",
                "#.###.###.#",
                "#...#.....#",
                "#.#.#######",
                "#.#...#...#",
                "#####.###.#",
                "#...#.....#",
                "#.#######.#",
                "#..........",
                "###########",
        };
        int[][] expecteds = {
                {2},
                {0, 1},
                {10, 9}
        };
        int[][] actuals = mazeExits.solve(inputArea, inputStart, maze);
        assertArrayEquals(expecteds, actuals);
    }

    @Test
    public void testSolve4() {
        int[] inputArea = {11, 11};
        int[] inputStart = {5, 5};
        String[] maze = {
                "###########",
                "......#...#",
                "#.###.###.#",
                "#...#.....#",
                "#.#.#.##.##",
                "#.#...#...#",
                "#.###.###.#",
                "#...#.....#",
                "#.###.###.#",
                "#.........#",
                "###########"
        };
        int[][] expecteds = {
                {1},
                {0, 1}
        };
        int[][] actuals = mazeExits.solve(inputArea, inputStart, maze);
        assertArrayEquals(expecteds, actuals);
    }
}
