package com.ty.coding_game;

import static org.junit.Assert.assertArrayEquals;

import org.junit.Test;

import coding_game.RubiksCube111;

public class RubiksCube111Test {

	RubiksCube111 cube = new RubiksCube111();
	
	@Test
	public void TestRotation1() {
		String[] input = new String[] {"z", "D", "L"};
		assertArrayEquals(new char[] {'L', 'U'}, cube.rotation(input));
	}
	
	@Test
	public void TestRotation2() {
		String[] input = new String[] {"x", "D", "L"};
		assertArrayEquals(new char[] {'F', 'L'}, cube.rotation(input));
	}
	
	@Test
	public void TestRotation3() {
		String[] input = new String[] {"z'", "D", "L"};
		assertArrayEquals(new char[] {'R', 'D'}, cube.rotation(input));
	}
	
	@Test
	public void TestRotation4() {
		String[] input = new String[] {"y", "D", "L"};
		assertArrayEquals(new char[] {'D', 'B'}, cube.rotation(input));
	}
	
	@Test
	public void TestRotation5() {
		String[] input = new String[] {"z z' y'", "D", "L"};
		assertArrayEquals(new char[] {'D', 'F'}, cube.rotation(input));
	}
	
	@Test
	public void TestRotation6() {
		String[] input = new String[] {"x y'", "D", "L"};
		assertArrayEquals(new char[] {'R', 'F'}, cube.rotation(input));
	}
	
	@Test
	public void TestRotation7() {
		String[] input = new String[] {"y'", "D", "L"};
		assertArrayEquals(new char[] {'D', 'F'}, cube.rotation(input));
	}
	
//	@Test
//	public void TestRotationManual() {
//		String[] input = new String[] {"y'", "D", "L"};
//		assertArrayEquals(new char[] {'D', 'F'}, cube.rotationManual());
//	}
}
