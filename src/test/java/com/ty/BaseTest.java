package com.ty;
import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.rules.TestName;

import java.util.concurrent.TimeUnit;

public class BaseTest{
//    @Rule
//    public MyStopWatch myStopWatch = new MyStopWatch();
	@Rule
    public TestName testName = new TestName();
    long timeStart, timeEnd, timeExpend;

    @Before
    public void before() {
        System.out.println(testName.getMethodName() + ": ");
        timeStart = System.nanoTime();
    }

    @After
    public void after() {
        timeEnd = System.nanoTime();
        timeExpend = timeEnd - timeStart;
        System.out.println("spent " + (timeExpend/1000) + " ms");
//        System.out.println(TimeUnit.NANOSECONDS.toMicros(timeExpend));
    }
}
