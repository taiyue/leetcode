package com.ty;
import easy.NextGreaterElement;
import org.junit.*;
import org.junit.runners.MethodSorters;

import static org.junit.Assert.*;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class NextGreaterElementTest extends BaseTest {
    NextGreaterElement nextGreaterElement = new NextGreaterElement();
    final int[] sampleSubNums1 = {4, 1, 2};
    final int[] sampleNums1 = {1, 3, 4, 2};
    final int[] sampleSubNums2 = {2, 4};
    final int[] sampleNums2 = {1, 2, 3, 4};
    final int[] sampleSubNums3 = {1,3,5,2,4};
    final int[] sampleNums3 = {6,5,4,3,2,1,7};
    final int[] sampleSubNums4 = {2, 4};
    final int[] sampleNums4 = {10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59, 60, 61, 62, 63, 64, 65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82, 83, 84, 85, 86, 87, 88, 89, 90, 91, 92, 93, 94, 95, 96, 97, 98, 99,
            1, 2, 3, 4};

    @Test
    public void baseTest() {
//        assertArrayEquals(new int[]{-1,3,-1}, nextGreaterElement.base(sampleSubNums1, sampleNums1));
//        assertArrayEquals(new int[]{3,-1}, nextGreaterElement.base(sampleSubNums2, sampleNums2));
        assertArrayEquals(new int[]{7,7,7,7,7}, nextGreaterElement.base(sampleSubNums3, sampleNums3));
    }

    @Test
    public void baseImprovedCacheTest() {
//        assertArrayEquals(new int[]{-1,3,-1}, nextGreaterElement.base(sampleSubNums1, sampleNums1));
//        assertArrayEquals(new int[]{3,-1}, nextGreaterElement.base(sampleSubNums2, sampleNums2));
        assertArrayEquals(new int[]{7,7,7,7,7}, nextGreaterElement.baseImprovedCache(sampleSubNums3, sampleNums3));
    }

    @Test
    public void baseImprovedTest() {
//        assertArrayEquals(new int[]{-1,3,-1}, nextGreaterElement.base(sampleSubNums1, sampleNums1));
//        assertArrayEquals(new int[]{3,-1}, nextGreaterElement.base(sampleSubNums2, sampleNums2));
        assertArrayEquals(new int[]{7,7,7,7,7}, nextGreaterElement.baseImproved(sampleSubNums3, sampleNums3));
    }
}
