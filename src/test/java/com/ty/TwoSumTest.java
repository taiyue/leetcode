package com.ty;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.ty.model.Data;
import com.ty.model.NumSet;

import easy.TwoSum;

import org.junit.BeforeClass;
import org.junit.Test;

import java.io.File;
import java.io.IOException;

import static org.junit.Assert.assertEquals;

public class TwoSumTest extends BaseTest{
    TwoSum twoSum = new TwoSum();
    static NumSet[] numSets, numSetsForCount;

//    public void twoSumTest() {
//        for (NumSet numSet : numSets) {
//            assertEquals(numSet.getExpectedPosition(), twoSum.twoSum(numSet.getNums(), numSet.getTarget()));
//        }
//    }

    @Test
    public void countTwoSumTest() {
        for (NumSet numSet : numSetsForCount) {
            assertEquals(numSet.getExpectedCount(), twoSum.countTwoSum(numSet.getNums(), numSet.getTarget()));
        }
//        assertEquals(3, twoSum.countTwoSum(new int[]{3, 3, 3, 2, 1, 88, 9, 33, 77}, 5));
    }

    @Test
    public void countTwoSumImprovedTest() {
        for (NumSet numSet : numSetsForCount) {
            assertEquals(numSet.getExpectedCount(), twoSum.countTwoSumImproved(numSet.getNums(), numSet.getTarget()));
        }
//        assertEquals(3, twoSum.countTwoSumImproved(nums, 5));
//        assertEquals(3, twoSum.countTwoSumImproved(new int[]{3, 3, 3, 2, 1, 88, 9, 33, 77}, 5));
    }

    @BeforeClass
    public static void beforeClass() {
        String filePathName = "src/test/resources/data.json";
        String filePathNameForCount = "src/test/resources/dataForCount.json";
        ObjectMapper mapper = new ObjectMapper();
        mapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);
        try {
            Data data = mapper.readValue(new File(filePathName), Data.class);
            numSets = data.getNumSets();
            data = mapper.readValue(new File(filePathNameForCount), Data.class);
            numSetsForCount = data.getNumSets();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
