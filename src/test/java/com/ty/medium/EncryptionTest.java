package com.ty.medium;

import org.junit.Test;

import medium.Encryption;

import static org.junit.Assert.*;

public class EncryptionTest {
	
	@Test public void testEncryption() {
		Encryption enc = new Encryption();
		String testPattern1 = "haveaniceday";
		String expect1 = "hae and via ecy";
		String rs1 = enc.encryption(testPattern1);
		assertEquals("encryption test1", expect1, rs1);
		
		String testPattern2 = "feedthedog";
		String expect2 = "fto ehg ee dd";
		String rs2 = enc.encryption(testPattern2);
		assertEquals("encryption test2", expect2, rs2);
	}
}
