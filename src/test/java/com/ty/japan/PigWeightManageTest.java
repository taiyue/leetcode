package com.ty.japan;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import com.ty.BaseTest;
import com.ty.util.TestCase;

import japan.PigWeightManage;

public class PigWeightManageTest extends BaseTest {
	
	TestCase<int[][], Integer> testCase1 = new TestCase<>(
			new int[][]{
				{2, 10, 20},
				{5, 6},
				{3, 10}
			}, 3);
	
	TestCase<int[][], Integer> testCase2 = new TestCase<>(
			new int[][]{
				{8, 300, 400},
				{9, 39},
				{48, 38},
				{21, 10},
				{14, 45},
				{32, 20},
				{32, 48},
				{9, 7},
				{19, 16}
			}, 194);
	
	TestCase<int[][], Integer> testCase3 = new TestCase<>(
			new int[][]{
				{4, 300, 309},
				{9, 39},
				{48, 38},
				{21, 10},
				{14, 45}
			}, 4);
	
	TestCase<int[][], Integer> testCase4 = new TestCase<>(
			new int[][]{
				{3, 900000000, 1000000000},
				{3000, 300000000},
				{3000, 200000000},
				{3000, 300000000}
			}, 1);
	
	TestCase<int[][], Integer> testCase5 = new TestCase<>(
			new int[][]{
				{20, 3000, 1000000000},
				{14, 1}, {14, 2}, {14, 3}, {14, 4}, {14, 5},
				{14, 6}, {14, 7}, {14, 8}, {14, 9}, {14, 10},
				{14, 11}, {14, 12}, {14, 13}, {14, 14}, {14, 15},
				{14, 16}, {14, 17}, {14, 18}, {14, 19}, {14, 20}
			}, 1048576);
	
	TestCase<int[][], Long> testCase6 = new TestCase<>(
			new int[][]{
				{35, 3000, 1000000000},
				{14, 1}, {14, 2}, {14, 3}, {14, 4}, {14, 5},
				{14, 6}, {14, 7}, {14, 8}, {14, 9}, {14, 10},
				{14, 11}, {14, 12}, {14, 13}, {14, 14}, {14, 15},
				{14, 16}, {14, 17}, {14, 18}, {14, 19}, {14, 20},
				{14, 16}, {14, 17}, {14, 18}, {14, 19}, {14, 20},
				{14, 16}, {14, 17}, {14, 18}, {14, 19}, {14, 20},
				{14, 16}, {14, 17}, {14, 18}, {14, 19}, {14, 20},
			}, 34359738368L);
	
	@Test public void TestPigWeightManage() {
		PigWeightManage pwm = new PigWeightManage();
//		int rs1 = pwm.pigWeightMange(this.testCase1.getTest());
//		assertEquals("test1: ", this.testCase1.getExpect().intValue(), rs1);
//		
//		int rs2 = pwm.pigWeightMange(this.testCase2.getTest());
//		assertEquals("test2: ", this.testCase2.getExpect().intValue(), rs2);
//		
//		int rs3 = pwm.pigWeightMange(this.testCase3.getTest());
//		assertEquals("test3: ", this.testCase3.getExpect().intValue(), rs3);
		
//		int rs4 = pwm.pigWeightMange(this.testCase4.getTest());
//		assertEquals("test4: ", this.testCase4.getExpect().intValue(), rs4);
		
//		int rs5 = pwm.pigWeightMange(this.testCase5.getTest());
//		assertEquals("test5: ", this.testCase5.getExpect().intValue(), rs5);
	}
	
	@Test public void TestPigWeightMangeImprove() {
		PigWeightManage pwm = new PigWeightManage();
		
//		long rs1 = pwm.pigWeightMangeImprove(this.testCase1.getTest());
//		assertEquals("test1: ", this.testCase1.getExpect().longValue(), rs1);
		
		long rs2 = pwm.pigWeightMangeImprove(this.testCase2.getTest());
		assertEquals("test2: ", this.testCase2.getExpect().longValue(), rs2);
	}
}
