package com.ty.util;

public class TestCase<T, R> {
	
	private T test;
	private R expect;
	
	public TestCase(T test, R expect) {
		this.test = test;
		this.expect = expect;
	}
	
	public T getTest() {
		return test;
	}

	public void setTest(T test) {
		this.test = test;
	}

	public R getExpect() {
		return expect;
	}

	public void setExpect(R expect) {
		this.expect = expect;
	}
}
