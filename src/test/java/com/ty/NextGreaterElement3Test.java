package com.ty;
import easy.NextGreaterElement3;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class NextGreaterElement3Test extends BaseTest {

    NextGreaterElement3 tester = new NextGreaterElement3();

    final int sampleNum = 1849876543;
    final int sampleRs = 1853446789;

    final int sampleNum2 = 2038;
    final int sampleRs2 = 2083;

    final int sampleNum3 = 4984564;
    final int sampleRs3 = 4984645;

    final int sampleNum4 = 1999999999;
    final int sampleRs4 = -1;

//    @Test
    public void baseTest() {
        assertEquals(sampleRs, tester.base(sampleNum));
//        assertEquals(sampleRs4, tester.base(sampleNum4));
    }

    @Test
    public void baseImprovedTest() {
        assertEquals(sampleRs, tester.baseImproved(sampleNum));
//        assertEquals(sampleRs4, tester.baseImproved(sampleNum4));

    }
}
