package com.ty;
import easy.NextGreaterElement2;
import org.junit.Test;

import static org.junit.Assert.assertArrayEquals;

public class NextGreaterElement2Test extends BaseTest {
    NextGreaterElement2 nextGreaterElement2 = new NextGreaterElement2();

    final int[] sampleNums1 = {1, 2, 1};
    final int[] sampleRs1 = {2, -1, 2};

    @Test
    public void baseTest() {
        assertArrayEquals(sampleRs1, nextGreaterElement2.base(sampleNums1));
    }

    @Test
    public void baseImprovedTest() {
        assertArrayEquals(sampleRs1, nextGreaterElement2.baseImproved(sampleNums1));
    }

    @Test
    public void baseImproved2Test() {
        assertArrayEquals(sampleRs1, nextGreaterElement2.baseImproved2(sampleNums1));
    }
}
