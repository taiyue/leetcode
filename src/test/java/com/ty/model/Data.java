package com.ty.model;

public class Data {
    NumSet[] numSets;

    public NumSet[] getNumSets() {
        return numSets;
    }

    public void setNumSets(NumSet[] numSets) {
        this.numSets = numSets;
    }
}
