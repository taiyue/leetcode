package com.ty.model;

public class NumSet {
    int[] nums;
    int target;
    int[] expectedPosition;
    int expectedCount;

    public int[] getNums() {
        return nums;
    }

    public void setNums(int[] nums) {
        this.nums = nums;
    }

    public int getTarget() {
        return target;
    }

    public void setTarget(int target) {
        this.target = target;
    }

    public int[] getExpectedPosition() {
        return expectedPosition;
    }

    public void setExpectedPosition(int[] expectedPosition) {
        this.expectedPosition = expectedPosition;
    }

    public int getExpectedCount() {
        return expectedCount;
    }

    public void setExpectedCount(int expectedCount) {
        this.expectedCount = expectedCount;
    }
}
